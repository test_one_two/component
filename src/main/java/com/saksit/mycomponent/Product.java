/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksit.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setProice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductlist(){
        ArrayList<Product>list = new ArrayList<>();
        list.add(new Product(1,"เอสเพรสโซ่ 1",40,"1.jfif"));
        list.add(new Product(2,"เอสเพรสโซ่ 2",30,"2.jfif"));
        list.add(new Product(3,"เอสเพรสโซ่ 3",40,"3.jfif"));
        list.add(new Product(4,"อเมริกาโน่ 1",30,"1.jfif"));
        list.add(new Product(5,"อเมริกาโน่ 2",40,"2.jfif"));
        list.add(new Product(1,"อเมริกาโน่ 3",50,"3.jfif"));
        list.add(new Product(1,"ชาเย็น 1",40,"1.jfif"));
        list.add(new Product(1,"ชาเย็น 2",40,"2.jfif"));
        list.add(new Product(1,"ชาเย็น 3",40,"3.jfif"));

        return list;
        
    }
}
